from rest_framework import viewsets
from .models import song, album, artist
from .serializer import songSerializer, albumSerializer, artistSerializer

class songViewSet(viewsets.ModelViewSet):
	queryset = song.objects.all()
	serializer_class = songSerializer

class albumViewSet(viewsets.ModelViewSet):
	queryset = album.objects.all()
	serializer_class = albumSerializer

class artistViewSet(viewsets.ModelViewSet):
	queryset = artist.objects.all()
	serializer_class = artistSerializer