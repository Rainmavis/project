from rest_framework import serializers
from .models import song, album, artist

class songSerializer(serializers.ModelSerializer):
	class Meta:
		model = song
		fields = '__all__'
	duration = serializers.SerializerMethodField()
	album_str = serializers.SerializerMethodField()
	def get_duration(self, obj):
		return '{}:{:02d}:{:02d}'.format(obj.duration_hours, obj.duration_minutes, obj.duration_seconds)
	def get_album_str(self, obj):
		return '{} - {}'.format(obj.album.title, obj.album.artist)
class albumSerializer(serializers.ModelSerializer):
	class Meta:
		model = album
		fields = '__all__'
	artist_str = serializers.SerializerMethodField()
	def get_artist_str(self, obj):
		return '{}'.format(obj.artist.name)
class artistSerializer(serializers.ModelSerializer):
	class Meta:
		model = artist
		fields = '__all__'