from django.db import models
import datetime
from django.utils.translation import gettext as _

# Create your models here.

class song(models.Model):
	title = models.CharField(max_length = 255)
	duration_hours = models.PositiveIntegerField(default=0)
	duration_minutes = models.PositiveIntegerField(default=0)
	duration_seconds = models.PositiveIntegerField(default=0)
	album = models.ForeignKey(
		'songapp.album',
		on_delete = models.CASCADE,
	)


class album(models.Model):
	title = models.CharField(max_length = 255)
	publication_date = models.DateField(_("Date"), default = datetime.date.today)
	cover_image = models.ImageField(upload_to='images/cover_image/', default='images/cover_image/default_song_album.jpg')
	artist = models.ForeignKey(
		'songapp.artist',
		on_delete = models.CASCADE,
	)
	def __unicode__(self):
		return '{} - {}'.format(self.title, self.artist)

	def __str__(self):
		return '{} - {}'.format(self.title, self.artist)

class artist(models.Model)	:
	name = models.CharField(max_length = 255)

	def __unicode__(self):
		return '{}'.format(self.name)
	
	def __str__(self):
		return '{}'.format(self.name)
