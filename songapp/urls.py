from rest_framework import routers
from .viewsets import songViewSet, albumViewSet, artistViewSet

router = routers.SimpleRouter()
router.register('songs', songViewSet)
router.register('albums', albumViewSet)
router.register('artists', artistViewSet)

urlpatterns = router.urls
