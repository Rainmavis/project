import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'

import ListSong from '@/components/songfrontend/ListSong'
import ListAlbum from '@/components/songfrontend/ListAlbum'
import CreateAlbum from'@/components/songfrontend/CreateAlbum'

Vue.use(Router)

export default new Router({
  routes: [
    {
      	path: '/',
      	name: 'HelloWorld',
      	component: HelloWorld
    },
    {
      	path: '/songs',
      	name: 'ListSong',
      	component: ListSong
    },
    {
    	path: '/albums',
    	name: 'ListAlbum',
    	component: ListAlbum
    },
    {
    	path: '/albums/create',
    	name: 'CreateAlbum',
    	component: CreateAlbum
    }
  ],
  mode:'history'
})
